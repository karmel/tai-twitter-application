package pl.edu.agh.twitterApp.config;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;

@Configuration
public class TestContext {
	@Bean
	public AuthenticationProvider databaseAuthenticationProvider(){
		AuthenticationProvider provider = Mockito.mock(AuthenticationProvider.class);

//		when(provider.authenticate())
		return provider;
	}
}
