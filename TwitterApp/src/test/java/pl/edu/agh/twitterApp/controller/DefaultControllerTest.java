package pl.edu.agh.twitterApp.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.social.twitter.api.*;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.edu.agh.twitterApp.config.TestContext;
import pl.edu.agh.twitterApp.config.WebMvcConfig;
import pl.edu.agh.twitterApp.config.WebSecurityConfig;
import pl.edu.agh.twitterApp.dao.IUserDao;
import pl.edu.agh.twitterApp.entity.User;

import java.util.Arrays;
import java.util.Date;

import static junit.framework.Assert.assertEquals;
import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class, WebMvcConfig.class, WebSecurityConfig.class})
@WebAppConfiguration
public class DefaultControllerTest {
	@InjectMocks
	private DefaultController defaultController;

	private MockMvc mockMvc;

	@Mock
	private IUserDao userDao;

	@Mock
	private pl.edu.agh.twitterApp.util.SecurityContextFacade securityContextFacade;

	@Mock
	private SecurityContext context;

	@Mock
	private Authentication auth;

	@Mock
	private User user;

	@Mock
	private Twitter twitter;

	@Mock
	private TimelineOperations timelineOperations;

	@Mock
	private FriendOperations friendOperations;

	@Before
	public void setup(){
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(defaultController).build();
	}

	@Test
	public void testLogin() throws Exception{
		loggedOut();

		mockMvc.perform(get("/signin"))
			.andExpect(status().isOk())
			.andExpect(view().name("login"))
		;
	}

	@Test
	public void testDoubleLogin() throws Exception{
		loggedOut();

		mockMvc.perform(get("/signin"))
			.andExpect(status().isOk())
			.andExpect(view().name("login"))
		;

		loggedIn();

		mockMvc.perform(get("/signin"))
			.andExpect(status().is(302))
			.andExpect(redirectedUrl("/"))
		;
	}

	@Test
	public void testRedirectAfterLogin() throws Exception{
		loggedIn();

		mockMvc.perform(get("/signin"))
			.andExpect(status().is(302))
			.andExpect(redirectedUrl("/"))
		;
	}

	@Test
	public void testLogout() throws Exception{
		mockMvc.perform(get("/logout"))
			.andExpect(status().is(302))
			.andExpect(redirectedUrl("/"))
		;
	}

	@Test
	public void testMainPageDisplay() throws Exception{
		when(twitter.timelineOperations()).thenReturn(timelineOperations);
		Tweet tweet1 = new Tweet(1L, "test text", new Date(), "test", "", 2L, 1L, "pl", "");
		Tweet tweet2 = new Tweet(2L, "test2 text", new Date(), "test2", "", 2L, 1L, "pl", "");
		when(timelineOperations.getHomeTimeline()).thenReturn(Arrays.asList(tweet1, tweet2));
		when(securityContextFacade.getContext()).thenReturn(context);
		mockMvc.perform(get("/tweets")).andExpect(status().isOk());
	}

	@Test
	public void testFriendsList() throws Exception{
		loggedIn();
		setupTwitter();
		mockMvc.perform(get("/friends"))
			.andExpect(status().isOk()).andExpect(view().name("following"))
			.andExpect(model().attribute("model", hasEntry("current_menu", "following")))
			.andExpect(model().attribute("model", hasEntry(is("following"), hasSize(2))))
			.andExpect(model().attribute("model", hasEntry(is("following"), hasItem(
				allOf(
					hasProperty("id", is(100L)),
					hasProperty("screenName", is("name1"))
				)
			)))).andExpect(model().attribute("model", hasEntry(is("following"), hasItem(
			allOf(
				hasProperty("id", is(200L)),
				hasProperty("screenName", is("name2"))
			)
		))))

		;
	}

	@Test
	public void testAddFollower() throws Exception{
		loggedIn();
		setupTwitter();
		mockMvc.perform(post("/add-following").param("username", "testAccount"))
			.andExpect(status().is(302))
			.andExpect(view().name("redirect:/friends"));    //TODO check with model  ? is that really neded?
	}

	@Test
	public void testRemoveFollower() throws Exception{
		loggedIn();
		setupTwitter();
		mockMvc.perform(get("/remove-following").param("username", "testAccount"))
			.andExpect(status().is(302))
			.andExpect(view().name("redirect:/friends"));
	}

	@Test
	public void testTweetList() throws Exception{
		loggedIn();
		setupTwitter();

		mockMvc.perform(get("/"))
			.andExpect(status().isOk())
			.andExpect(view().name("index"))
			.andExpect(model().attribute("model", hasEntry("current_menu", "tweets")))
			.andExpect(model().attribute("model", hasEntry(is("tweets"), hasSize(2))))
			.andExpect(model().attribute("model", hasEntry(is("tweets"), hasItem(
				allOf(
					hasProperty("id", is(1L)),
					hasProperty("fromUser", is("test")),
					hasProperty("text", is("test text"))
				)
			))))
			.andExpect(model().attribute("model", hasEntry(is("tweets"), hasItem(
				allOf(
					hasProperty("id", is(2L)),
					hasProperty("fromUser", is("test2")),
					hasProperty("text", is("test2 text"))
				)
			))))
		;
	}

	private void setupTwitter(){
		//tweets
		when(twitter.timelineOperations()).thenReturn(timelineOperations);
		Tweet tweet1 = new Tweet(1L, "test text", new Date(), "test", "", 2L, 1L, "pl", "");
		Tweet tweet2 = new Tweet(2L, "test2 text", new Date(), "test2", "", 2L, 1L, "pl", "");
		when(timelineOperations.getHomeTimeline()).thenReturn(Arrays.asList(tweet1, tweet2));

		//profiles
		when(twitter.friendOperations()).thenReturn(friendOperations);
		TwitterProfile profile1 = new TwitterProfile(100L, "name1", "name1", "url1", "imgUrl1", "desc1", "location1", new Date());
		TwitterProfile profile2 = new TwitterProfile(200L, "name2", "name2", "url2", "imgUrl2", "desc2", "location2", new Date());
		CursoredList<TwitterProfile> profiles = new CursoredList<>(10, 0L, 1L);
		profiles.add(profile1);
		profiles.add(profile2);
		when(friendOperations.getFriends()).thenReturn(profiles);

		//adding
		when(friendOperations.follow(anyString())).thenAnswer(new Answer<Object>() {
			@Override
			public Object answer(InvocationOnMock invocation) throws Throwable{
				Object[] args = invocation.getArguments();
				assertEquals("", args[0], "testAccount");
				return args[0];
			}
		});

		//removing
		when(friendOperations.unfollow(anyString())).thenAnswer(new Answer<Object>() {
			@Override
			public Object answer(InvocationOnMock invocation) throws Throwable{
				Object[] args = invocation.getArguments();
				assertEquals("", args[0], "testAccount");
				return args[0];
			}
		});
	}

	private void loggedIn(){
		when(securityContextFacade.getContext()).thenReturn(context);
		when(context.getAuthentication()).thenReturn(auth);
		when(auth.getPrincipal()).thenReturn(user);
	}

	private void loggedOut(){
		when(securityContextFacade.getContext()).thenReturn(context);
		when(context.getAuthentication()).thenReturn(null);
	}
}
