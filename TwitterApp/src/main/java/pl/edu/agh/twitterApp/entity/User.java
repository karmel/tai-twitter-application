package pl.edu.agh.twitterApp.entity;

import com.google.common.base.Objects;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = User.ENTITY_NAME)
@Table(name = User.ENTITY_NAME)
public class User implements Serializable {
	public static final String ENTITY_NAME = "user";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = Columns.ID)
	private Long id;

	@Column(name = Columns.LOGIN, nullable = false, length = 50, unique = true)
	@NotBlank
	private String login;

	@Column(name = Columns.PASSWORD, nullable = false, length = 255)
	@NotBlank
	private String password;


	@Column(name = Columns.ISADMIN, nullable = false)
	private Boolean isAdmin;

	public Boolean isAdmin(){
		return isAdmin;
	}

	public void setAdmin(Boolean admin){
		isAdmin = admin;
	}


	public User(){
		/* empty */
	}

	public User(String userId){
		this.setId(Long.parseLong(userId));
	}

	public Long getId(){
		return id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getLogin(){
		return login;
	}

	public void setLogin(String login){
		this.login = login;
	}

	public String getPassword(){
		return password;
	}

	public void setPassword(String password){
		this.password = password;
	}

	@Override
	public int hashCode(){
		return id == null ? super.hashCode() : id.hashCode();
	}

	@Override
	public boolean equals(Object obj){
		return obj == this || obj instanceof User && Objects.equal(id, ((User)obj).id);
	}

	/**
	 * Column names for {@code User} entity.
	 */
	public static final class Columns {
		public static final String ID = "id";
		public static final String LOGIN = "login";
		public static final String PASSWORD = "password";
		public static final String ISADMIN = "is_admin";
	}
}
