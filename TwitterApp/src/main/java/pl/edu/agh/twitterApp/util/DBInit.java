package pl.edu.agh.twitterApp.util;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import pl.edu.agh.twitterApp.dao.IUserDao;
import pl.edu.agh.twitterApp.entity.User;

import javax.inject.Inject;

@Component
public class DBInit implements ApplicationListener<ContextRefreshedEvent> {
	@Inject
	private IUserDao userDao;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event){
		if(userDao.findAll().size() == 0){
			User user = new User();
			user.setLogin("admin");
			user.setPassword("admin");
			user.setAdmin(true);
			userDao.save(user);


			user = new User();
			user.setLogin("user");
			user.setPassword("user");
			user.setAdmin(false);
			userDao.save(user);
			userDao.flush();
		}
	}
}
