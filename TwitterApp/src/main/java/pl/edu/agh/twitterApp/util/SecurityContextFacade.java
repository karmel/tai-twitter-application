package pl.edu.agh.twitterApp.util;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class SecurityContextFacade {
	public org.springframework.security.core.context.SecurityContext getContext(){
		return SecurityContextHolder.getContext();
	}

	public void setContext(org.springframework.security.core.context.SecurityContext context){
		SecurityContextHolder.setContext(context);
	}
}
