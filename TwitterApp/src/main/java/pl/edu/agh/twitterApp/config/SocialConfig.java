package pl.edu.agh.twitterApp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.env.Environment;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.support.ConnectionFactoryRegistry;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.social.twitter.connect.TwitterConnectionFactory;

import javax.inject.Inject;

/**
 * Spring Social Configuration.
 */
@Configuration
public class SocialConfig {
	@Inject
	private Environment environment;

	/**
	 * When a new provider is added to the app, register its {@code ConnectionFactory} here.
	 * @see TwitterConnectionFactory
	 */
	@Bean
	public ConnectionFactoryLocator connectionFactoryLocator() {
		ConnectionFactoryRegistry registry = new ConnectionFactoryRegistry();
		registry.addConnectionFactory(new TwitterConnectionFactory(
			environment.getProperty("twitter.consumerKey"),
			environment.getProperty("twitter.consumerSecret")));
		return registry;
	}

	/**
	 * A proxy to a request object representing the application Twitter account.
	 */
	@Bean
	@Scope(value="request", proxyMode=ScopedProxyMode.INTERFACES)
	public Twitter twitter() {
		return new TwitterTemplate(
			environment.getProperty("twitter.consumerKey"),
			environment.getProperty("twitter.consumerSecret"),
			environment.getProperty("twitter.accessToken"),
			environment.getProperty("twitter.accessTokenSecret")
		);
	}
}
