package pl.edu.agh.twitterApp.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import pl.edu.agh.twitterApp.dao.IUserDao;
import pl.edu.agh.twitterApp.entity.User;
import pl.edu.agh.twitterApp.util.SecurityRole;

import java.util.ArrayList;
import java.util.List;

@Component
public class DatabaseAuthenticationProvider implements AuthenticationProvider {
	@Autowired
	IUserDao userDao;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException{
		Authentication auth = null;

		String name = authentication.getName();
		String password = authentication.getCredentials().toString();

		User u = userDao.findOneByLogin(name);

		if(u != null && u.getPassword().equals(password)){
			List<GrantedAuthority> grantedAuths = new ArrayList<>();
			grantedAuths.add(new SimpleGrantedAuthority(SecurityRole.USER_ROLE));

			if(u.isAdmin()){
				grantedAuths.add(new SimpleGrantedAuthority(SecurityRole.ADMIN_ROLE));
			}

			auth = new UsernamePasswordAuthenticationToken(name, password, grantedAuths);
		}

		return auth;
	}

	@Override
	public boolean supports(Class<?> authentication){
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
}