package pl.edu.agh.twitterApp.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import javax.inject.Inject;

/**
 * Spring Security configuration.
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private AuthenticationProvider databaseAuthenticationProvider;

	@Inject
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception{
		auth.authenticationProvider(databaseAuthenticationProvider);
	}

	@Override
	public void configure(WebSecurity web) throws Exception{
		web
			.ignoring()
			.antMatchers("/resources/**");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception{
		http
			// Login form
			.formLogin()
			.loginPage("/signin")
			.defaultSuccessUrl("/")
			.loginProcessingUrl("/signin")
			.usernameParameter("username")
			.passwordParameter("password")
			.permitAll()
			.and()
				// Disable CSRF checking
			.csrf()
			.disable()
				// Authorization
			.authorizeRequests()
			.antMatchers("/logout").permitAll()
			.anyRequest().authenticated();
	}
}
