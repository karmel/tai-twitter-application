package pl.edu.agh.twitterApp.util;

public class SecurityRole {
	public final static String USER_ROLE = "ROLE_USER";
	public final static String ADMIN_ROLE = "ROLE_ADMIN";
}
