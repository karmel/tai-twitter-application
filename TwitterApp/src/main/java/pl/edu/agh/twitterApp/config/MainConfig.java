package pl.edu.agh.twitterApp.config;

import com.google.common.collect.ImmutableMap;
import org.hibernate.dialect.HSQLDialect;
import org.hsqldb.Server;
import org.hsqldb.jdbcDriver;
import org.hsqldb.persist.HsqlProperties;
import org.hsqldb.server.ServerAcl;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.dao.support.PersistenceExceptionTranslator;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;
import java.io.IOException;

/**
 * Main configuration class for the application.
 * Turns on @Component scanning, loads externalized application.properties, and sets up the database.
 */
@Configuration
@ComponentScan(basePackages = "pl.edu.agh.twitterApp", excludeFilters = { @ComponentScan.Filter(Configuration.class) })
@PropertySource("classpath:application.properties")
@EnableTransactionManagement
@EnableJpaRepositories("pl.edu.agh.twitterApp.dao")
public class MainConfig {
	@Inject
	private Environment environment;

	/**
	 * Data source for repositories.
	 *
	 * @return bean instance
	 */
	@Bean
	@DependsOn("inMemoryDatabaseServer")
	public javax.sql.DataSource dataSource() {
		DriverManagerDataSource source = new DriverManagerDataSource(
			String.format("jdbc:hsqldb:hsql://%s:%s/%s", environment.getProperty("db.host"), environment.getProperty("db.port"), environment.getProperty("db.name")),
			environment.getProperty("db.username"), environment.getProperty("db.password"));
		source.setDriverClassName(jdbcDriver.class.getCanonicalName());
		return source;
	}

	/**
	 * Creates and starts the HSQLDB in-memory database server.
	 *
	 * @return bean instance
	 * @throws java.io.IOException
	 *             if something went wrong
	 * @throws ServerAcl.AclFormatException
	 *             if something went wrong
	 */
	@Bean
	public Server inMemoryDatabaseServer() throws IOException, ServerAcl.AclFormatException{
		Server server = new Server();
		server.setDatabaseName(0, environment.getProperty("db.name")); // this is the alias that the remote processes can use
		server.setDatabasePath(0, String.format("mem:%s;user=%s;password=%s", environment.getProperty("db.name"), environment.getProperty("db.username"), environment.getProperty("db.password")));
		server.setPort(Integer.parseInt(environment.getProperty("db.port")));
		HsqlProperties props = new HsqlProperties();
		props.setProperty("server.remote_open", "true"); // allow remote connections
		server.setProperties(props);
		server.start();
		return server;
	}

	/**
	 * JPA Vendor adapter.
	 *
	 * @return bean instance
	 */
	@Bean
	public JpaVendorAdapter jpaVendorAdapter() {
		HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
		adapter.setDatabasePlatform(HSQLDialect.class.getCanonicalName());
		adapter.setShowSql(true);
		return adapter;
	}

	/**
	 * Entity manager factory.
	 *
	 * @return bean instance
	 */
	@Bean
	public EntityManagerFactory entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setDataSource(dataSource());
		factory.setJpaVendorAdapter(jpaVendorAdapter());
		factory.setPackagesToScan("pl.edu.agh.twitterApp.entity");
		factory.setJpaPropertyMap(ImmutableMap.of(
			"hibernate.hbm2ddl.auto", "create-drop",
			"hibernate.dialect", HSQLDialect.class.getCanonicalName()
		));

		// needs to be called manually; the problem is described here:
		// http://forum.springsource.org/showthread.php?54128-Correct-Usage-of-FactoryBeans-in-JavaConfig
		factory.afterPropertiesSet();
		return factory.getObject();
	}

	/**
	 * Transaction manager.
	 *
	 * @return bean instance
	 */
	@Bean
	public PlatformTransactionManager transactionManager() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory());
		transactionManager.setDataSource(dataSource());
		return transactionManager;
	}

	/**
	 * Exceptions translator processor.
	 *
	 * @return bean instance
	 */
	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	/**
	 * The exceptions translator for persistence layer.
	 *
	 * @return bean instance
	 */
	@Bean
	public PersistenceExceptionTranslator hibernateExceptionTranslator() {
		return new PersistenceExceptionTranslator() {
			@Override
			public DataAccessException translateExceptionIfPossible(RuntimeException ex) {
				throw ex;
			}
		};
	}
}
