package pl.edu.agh.twitterApp.util;

import org.springframework.social.twitter.api.TwitterProfile;

public class UserSearch {
	private String id;
	private String text;

	public UserSearch(TwitterProfile twitterProfile) {
		this.id = twitterProfile.getScreenName();
		this.text = String.format("%s %s", twitterProfile.getScreenName(), twitterProfile.getName());
	}

	public String getId() {
		return id;
	}

	public String getText() {
		return text;
	}
}
