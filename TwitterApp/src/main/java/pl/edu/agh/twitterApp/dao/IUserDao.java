package pl.edu.agh.twitterApp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.edu.agh.twitterApp.entity.User;

/**
 * Repository for User Entity.
 *
 * @author Amadeusz Starzykiewicz
 */
public interface IUserDao extends JpaRepository<User, Long> {
	/**
	 * Find user in database using its login.
	 *
	 * @param login User's login
	 * @return User or {@code null} if not found.
	 */
	public User findOneByLogin(String login);
}
