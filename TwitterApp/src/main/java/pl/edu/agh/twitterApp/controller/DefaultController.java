package pl.edu.agh.twitterApp.controller;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.springframework.security.access.annotation.Secured;
import org.springframework.social.ApiException;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.TwitterProfile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import pl.edu.agh.twitterApp.dao.IUserDao;
import pl.edu.agh.twitterApp.entity.User;
import pl.edu.agh.twitterApp.util.SecurityContextFacade;
import pl.edu.agh.twitterApp.util.UserSearch;

import javax.inject.Inject;
import java.util.List;

@Controller
@RequestMapping("/")
public class DefaultController {
	private final static String INDEX_PAGE = "index";
	private final static String LOGIN_PAGE = "login";
	private final static String FOLLOWING_PAGE = "following";

	@Inject
	private SecurityContextFacade securityContextFacade;

	@Inject
	private IUserDao userDao;

	@Inject
	private Twitter twitter;

	@RequestMapping(value = "signin")
	public String login(){
		if(securityContextFacade.getContext().getAuthentication() != null && securityContextFacade.getContext().getAuthentication().getPrincipal() instanceof User){
			return "redirect:/";
		}

		return LOGIN_PAGE;
	}

	@RequestMapping(value = "logout")
	@Secured("ROLE_USER")
	public String logout(){
		return "redirect:/";
	}

	@RequestMapping(value = {"", "tweets"}, method = RequestMethod.GET)
	@Secured("ROLE_USER")
	public String index(@ModelAttribute("model") ModelMap model){
		model.addAttribute("tweets", twitter.timelineOperations().getHomeTimeline());
		model.addAttribute("current_menu", "tweets");
		return INDEX_PAGE;
	}

	@RequestMapping(value ={"friends"}, method = RequestMethod.GET)
	public String listFollowing(@ModelAttribute("model") ModelMap model){
		model.addAttribute("following", twitter.friendOperations().getFriends());
		model.addAttribute("current_menu", "following");
		return FOLLOWING_PAGE;
	}

	@RequestMapping(value="/query/", method = RequestMethod.GET)
	public @ResponseBody List<UserSearch> getUsersByQuery(@RequestParam("query") String query){
		return Lists.transform(twitter.userOperations().searchForUsers(query), new Function<TwitterProfile, UserSearch>() {
			@Override
			public UserSearch apply(TwitterProfile profile){
				return new UserSearch(profile);
			}
		});
	}

	@RequestMapping(value = "add-following", method = RequestMethod.POST)
	@Secured("ROLE_ADMIN")
	public String addFollowing(@ModelAttribute("model") ModelMap model, @RequestParam("username") String username){
		try {
			twitter.friendOperations().follow(username);
		} catch(ApiException e){
			model.addAttribute("error_message", e.getMessage());
			return listFollowing(model);
		}

		return "redirect:/friends";
	}

	@RequestMapping(value = "remove-following", method = RequestMethod.GET)
	@Secured("ROLE_ADMIN")
	public String removeFollowing(@ModelAttribute("model") ModelMap model, @RequestParam("username") String username){
		try {
			twitter.friendOperations().unfollow(username);
		} catch(ApiException e){
			model.addAttribute("error_message", e.getMessage());
			return listFollowing(model);
		}

		return "redirect:/friends";
	}
}