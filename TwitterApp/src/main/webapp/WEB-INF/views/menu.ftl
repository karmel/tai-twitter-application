<#import "spring.ftl" as spring />
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
				<div class="navbar-header">
						<a class="navbar-brand" href="<@spring.url "/" />">Twitter App</a>
				</div>
				<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
								<li<#if model.current_menu == 'tweets'> class="active"</#if>><a href="<@spring.url '/tweets' />">Tweets</a></li>
								<li<#if model.current_menu == 'following'> class="active"</#if>><a href="<@spring.url '/friends' />">Following</a></li>
								<li><a href="<@spring.url "/logout" />">Logout</a></li>
						</ul>
				</div>
		</div>
</div>