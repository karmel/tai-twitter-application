<#import "spring.ftl" as spring />
<#macro layout>
<html>
<head>
	<title>Twitter App</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="UTF-8">
	<!-- Bootstrap core CSS -->
	<link href="<@spring.url '/resources/css/bootstrap.min.css'/>" rel="stylesheet">
	<link href="<@spring.url '/resources/css/select2.css'/>" rel="stylesheet">
	<link href="<@spring.url '/resources/css/select2-bootstrap.css'/>" rel="stylesheet">
	<link href="<@spring.url '/resources/css/styles.css'/>" rel="stylesheet">

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<#nested/>
	<script src="<@spring.url '/resources/js/bootstrap.js'/>"></script>
	<script src="<@spring.url '/resources/js/select2.min.js'/>"></script>
</body>
</html>
</#macro>