<#import "base.ftl" as base>
<#import "spring.ftl" as spring />
<#assign security=JspTaglibs["http://www.springframework.org/security/tags"] />
<@base.layout>
<#include "menu.ftl" />
<div class="container">
    <div class="jumbotron">
        <h1>List of people you are following</h1>
    </div>
		<#if model.error_message??>
        <div class="alert alert-danger">${model.error_message}</div>
		</#if>
		<@security.authorize access="hasRole('ROLE_ADMIN')">
		<form action="<@spring.url '/add-following' />" method="POST">
				<div class="input-group">
						<input type="text" class="form-control" id="query" name="username">
						<span class="input-group-btn">
								<button class="btn btn-default" type="submit">Add!</button>
						</span>
				</div>
    </form>
		</@security.authorize>
    <ul class="list-group">
			<#list model.following as item>
          <li class="list-group-item">
              <h4>${item.name}</h4>
							<@security.authorize access="hasRole('ROLE_ADMIN')">
              	<a href="<@spring.url '/remove-following?username=${item.screenName}' />"><span class=".glyphicon .glyphicon-remove">Remove</span></a>
							</@security.authorize>
          </li>
			</#list>
    </ul>
</div>
<@security.authorize access="hasRole('ROLE_ADMIN')">
<script type="text/javascript">
	jQuery(document).ready(function(){
			jQuery("#query").select2({
					placeholder: "Search for user to follow…",
					minimumInputLength: 3,
					ajax: {
							url: "/query/",
							dataType: 'json',
							quietMillis: 100,
							data: function (query) {
									return {
											query: query
									};
							},
							results: function (data) {
									return {results: data};
							}
					}
			});
  });
</script>
</@security.authorize>
</@base.layout>