<#import "spring.ftl" as spring />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Twitter App - Sign in</title>

    <!-- Bootstrap core CSS -->
    <link href="<@spring.url '/resources/css/bootstrap.min.css'/>" rel="stylesheet">
    <link href="<@spring.url '/resources/css/signin.css'/>" rel="stylesheet">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
<#if Session.SPRING_SECURITY_LAST_EXCEPTION?? && Session.SPRING_SECURITY_LAST_EXCEPTION.message?has_content>
    <div class="alert alert-danger alert-login">Login error</div>
</#if>
    <form class="form-signin" action="<@spring.url '/signin'/>" method="POST">
        <h2 class="form-signin-heading">Please sign in</h2>
        <input type="text" class="form-control" placeholder="Login" required autofocus name="username" />
        <input type="password" class="form-control" placeholder="Password" required name="password" />
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>
</div>
<script src="<@spring.url '/resources/js/bootstrap.js'/>"></script>
</body>
</html>