<#import "base.ftl" as base>
<#import "spring.ftl" as spring />
<@base.layout>
<#include "menu.ftl" />
<div class="container">
    <div class="jumbotron">
        <h1>List of tweets</h1>
    </div>
    <ul class="list-group">
			<#list model.tweets as item>
        <li class="list-group-item">
						<h4>${item.fromUser}</h4>
						<p>${item.text}</p>
				</li>
			</#list>
    </ul>
</div>
</@base.layout>